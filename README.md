# TurretsMod



This mod allows you to pick up vanilla autoturrets and shotgun turrets.  Note turrets must be in landclaim range to allow pickup.  This can be adjusted in multiplayer settings in case you find that you have placed a turret outside of the default range.  Just hold the "E" key and take your turret. Enjoy :)
